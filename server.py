#!/usr/bin/python3


import socketserver
import sys
import time
import json


class SIPRegisterHandler(socketserver.DatagramRequestHandler):

    """creo el diccionario."""
    dicc = {}

    """esto no estoy segura si va."""
    def json2registered(self):
        """si el fichero registered.json existe leo el contenido
        y uso el diccionario"""
        try:
            with open("registered.json", "r+b") as fichjson2:
                # print('sdfghgfxdfgfddfghgfxvbn')
                self.dicc = json.load(fichjson2)
                # load para decodif lo del fich json
        except FileNotFoundError:
            pass
            print('paso del fichero json')

    # escribo en el fich json.
    def register2json(self):
        """escribo en un fichero json"""
        with open("registered.json", "w") as fichjson:
            json.dump(self.dicc, fichjson)

    def handle(self):
        """
        handle method of the server class
        (all requests will be handled by this method)
        """
        self.json2registered()
        self.wfile.write(b"SIP/2.0 200 OK\r\n\r\n")
        IP_Cliente = self.client_address[0]
        Port_Cliente = self.client_address[1]
        for line in self.rfile:
            """me ecribe los datos IP y puero del cliente."""
            print("El cliente con IP " + str(IP_Cliente))
            print("y puerto: " + str(Port_Cliente))
            print("Nos envia: ", line.decode('utf-8'))

            """lo que metemos por el terminal del cliente."""
            registro = line.decode('utf-8').split()
            # print(registro) #para que me escriba registro y ver el orden

            """defino las variables para el expires."""
            tiempo = int(registro[4])
            tiempoeptiracion = time.time() + tiempo
            expirestime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(tiempoeptiracion))
            nowtime = time.strftime('%Y-%m-%d %H:%M:%S', time.gmtime(time.time()))

            """escribimos en el fichero los datos del register."""
            if registro[0] == "REGISTER":
                direcc = line.decode('utf-8').split(':')[1]
                direcc = direcc.split('SIP')[-2]
                ip = self.client_address[0]
                datos = ip
                self.dicc[direcc] = [datos, expirestime]

            """si el tiempo de expiracion es 0 borramos el diccionario."""
            if registro[3] == "Expires:":
                # print("asdfghjkasdfghjkasdfghjsdfghj")
                if tiempo == 0:
                    # print("va")
                    del self.dicc[direcc]
                    print("borramos diccionario")

                """
                lee la expiración del diccinario en el
                json y elimina los caducados."""
                for list in self.dicc.copy():
                    if nowtime >= self.dicc[list][1]:
                        del self.dicc[list]

            else:
                print("Usage: client.py ip puerto register sip_address expires_value")

            self.register2json()
            print("Guarda: ", self.dicc)


if __name__ == "__main__":

    PORT = int(sys.argv[1])
    serv = socketserver.UDPServer(('', PORT), SIPRegisterHandler)

    print("Lanzando servidor UDP de eco...")
    print()
    try:
        serv.serve_forever()
    except KeyboardInterrupt:
        print("Finalizado servidor")
